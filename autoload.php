<?php
/**
 * ACF ContentPress Frontend (ACFCPFE)
 * Allows for the easy display of ACF Fields using templates
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
defined( 'ABSPATH' ) or die();
spl_autoload_register(function($klass){
    if( strpos($klass, 'acfcontentpressfrontend') === 0 ){
		$klass = str_replace('acfcontentpressfrontend', '', $klass);
		$klass = dirname(__FILE__) . '/' .str_replace('\\', '/', strtolower($klass)) . '.php';

		if( file_exists($klass) ){
			require_once $klass;
		}
	}
});
