# README #

### What's up? ###

* Render Advanced Custom Fields fields and field groups using templates

### Installation ###

* Using composer or download and move to your plugin directory

NOTE: ACF & acfcontentpress is required for this to work. ACF PRO recommended to use Flexible Content Fields and other good stuff.

### Usage ###

1. Create a new directory `templates` in your active theme directory.
3. Add directories in `contents` for `fieldgroups`, `fields` and `layouts` as needed. 
4. Create a seperate file for each field/group/layout in their respective directories.
5. Render a field/group by calling `echo display( $key [, $id] );`

#### Template ####

Use `$this->$fieldname` to access values.

#### Manipulate data before displaying ####

Create a function `public function process($data, $id = null)` on your field/fieldgroup/layout class and return manipulated `$data`.