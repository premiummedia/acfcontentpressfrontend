<?php
/**
 * ACF ContentPress Frontend (ACFCPFE)
 * Allows for the easy display of ACF Fields using templates
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpressfrontend\core;
defined( 'ABSPATH' ) or die();

class Loader{

    /*
     * Todo: Remove
     * public static function load($fieldKey, $id = null){

		if( $id == null ){
            global $post;
            $id = $post->ID;
		}

		$fieldKey = apply_filters('acfcp/loadFieldKey', $fieldKey);

		$acfData = get_field($fieldKey, $id);

		$acfData = apply_filters('acfcp/get_field', $acfData, $fieldKey, $id);

		return apply_filters('acfcp/loadFieldData', $acfData);

    }*/

	public static function findContent($key){

        $contents = apply_filters('acfcp/contents', array());

        $keyParts = explode(".", $key);

		if( sizeof($keyParts) > 2 ){
			throw new \Exception('Content too deep to be found.');
		}

        $remaining = $contents[array_shift($keyParts)];

        foreach( $keyParts as $kp ){
            if( $remaining && $remaining->fields ){
                $remaining = $remaining->fields->get( $kp );
            }
        }

        return $remaining;

    }

}
