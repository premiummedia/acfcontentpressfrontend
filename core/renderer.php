<?php
/**
 * ACF ContentPress Frontend (ACFCPFE)
 * Allows for the easy display of ACF Fields using templates
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpressfrontend\core;
defined( 'ABSPATH' ) or die();

class Renderer{

    public $id;

    protected $data;
    protected $template;
    protected $frontend;

    public function __construct($template, $data, $id = null){
        $this->template = $template;
        $this->data = $data;
        $this->id = $id;

        if( !array_key_exists('id', $this->data) ){
            $this->data['id'] = $this->id;
        }

    }

    public function render(){
        ob_start();
        $context = \apply_filters('acfcpfe/contentcontext', []);
        extract($context, EXTR_SKIP);
        include $this->template;
        return ob_get_clean();
    }

    public function __get($key){

        $value = null;

        if( array_key_exists($key, $this->data) ){
            $value = $this->data[$key];
        }

        $value = \apply_filters('acfcpfe/contentvalue', $value, $this->data, $key);

        return $value;
    }

    public function __isset($key){
        return !!$this->__get($key);
        //return array_key_exists($key, $this->data);
    }

}
