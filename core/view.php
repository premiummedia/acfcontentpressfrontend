<?php
/**
 * ACF ContentPress Frontend (ACFCPFE)
 * Allows for the easy display of ACF Fields using templates
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpressfrontend\core;
defined( 'ABSPATH' ) or die();

use acfcontentpressfrontend\core\Renderer;

class View{

    public static function render($content, $id = null, $templateExt = '', $context = array()){

        $contentData = self::collectData($content, $id); 

        if( $contentData['type'] == 'field'){

            return self::callRenderer($content->getKey(), $contentData['data'], $id, $contentData['type'], $templateExt, $context);

        }else if( $contentData['type'] == 'fieldgroup' ){

            $fgData = array();

            foreach( $contentData['data'] as $fieldKey => $fieldData ){
                if( is_array($fieldData) && array_key_exists('data', $fieldData) ){
                    $fgData[$fieldKey] = $fieldData['data'];
                }else{
                    $fgData[$fieldKey] = $fieldData;
                }
            }
            return self::callRenderer($content->getKey(), $fgData, $id, 'fieldgroup', $templateExt, $context );

        }else if( $contentData['type'] == 'flex' ){

            return array_reduce(
                $contentData['data'],
                function($carry, $item) use ($id, $context){
                    $carry .= self::callRenderer( $item['acf_fc_layout'], $item, $id, 'layout', '', $context);
                    return $carry;
                },
                ""
            );

        }

    }

    public static function shortenKeys($data){

        $return = array();

        if( !is_array($data) ){
            return $data;
        }

        foreach( $data as $key => $value ){
            if( is_array( $value ) ){
                $value = self::shortenKeys($value);
            }
            $keyParts = explode(".", $key);
            $lastKey = end($keyParts);

            $return[$lastKey] = $value;
        }

        return $return;

    }

    public static function prepareData($data, $id = null){
        $data = self::shortenKeys($data);
        if(!$id){
            $id = get_the_ID();
        }
        if( $id && !array_key_exists('id', $data) ){
            $data['id'] = $id;
        }

        if( !array_key_exists('title', $data) && array_key_exists('id', $data) ){
            $title = get_the_title($data['id']);
            if( !empty($title) ){
                $data['title'] = $title;
            }
        }
        return $data;
    }

    public static function callRenderer( $key, $data, $id, $type, $templateExt = '', $context ){
        $template = self::findMatchingFile(explode(".", $key), $type, $templateExt);
        $data = self::prepareData($data, $id);
        if( is_array($context) && !empty($context)){
            $data = array_merge($data, $context);
        }
        $renderer = new Renderer($template, $data, $id);
        return $renderer->render();
    }

    public static function collectData($content, $id){
        return $content->getProcessedData($id);
    }


    /**
     * Finds a template file
     *
     * Template files are used in order for content key: standard.sidebar.paragraph
     * 1. standard.sidebar.paragraph.php
     * 2. sidebar.paragraph.php
     * 3. paragraph.php
     *
     * @param  [type] $nameParts [description]
     * @return [type]            [description]
     */
    public static function findMatchingFile($nameParts, $type = "field", $templateExt = ''){
        $base = get_template_directory()."/templates/".$type."s/";
		$search = array();
        $ext = '';
        if( $templateExt ){
            $ext = '-'.$templateExt;
        }
        for($i = 0; $i < sizeof($nameParts); $i++){
            $use = implode(".", array_slice($nameParts, $i));
            $lookFor = $base.$use.$ext.".php";
			array_push($search, $lookFor);
            if( file_exists( $lookFor ) ){
                return $lookFor;
            }
        }
        trigger_error("Template for ".$nameParts[sizeof($nameParts)-1]." not found. Searched for: ".implode($search, ", "));
        return $base.'default.php';
    }

}
