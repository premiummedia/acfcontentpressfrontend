<?php
/**
 * @package ACF ContentPress Frontend
 * ACF ContentPress Frontend (ACFCPFE)
 * Allows for the easy display of ACF Fields using templates
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
/*
Plugin Name: ACF ContentPress Frontend
Plugin URI: https://premiummedia.ch/acf-contentpress-frontend
Description: Display ACF Field(Group)s using templates.
Version: 1.2.0
Author: premiummedia.ch
License: GPLv2 or later
Text Domain: acf-contentpress-frontend
*/
defined( 'ABSPATH' ) or die();
require_once "autoload.php";

use acfcontentpressfrontend\core\View;
use acfcontentpressfrontend\core\Loader;

/**
 * Global function to display ACF/ACFCP Components
 *
 * $key string The key of the component to display
 * $id int The id of the post
 * $templateExt string A custom string to be appended to the template filename
 * $context array Additional data to be passed to the template
 */
function display($key, $id = null, $templateExt = '', $context = array()){

    $content = Loader::findContent($key);

    if( !$content ){
        trigger_error("Content with key [".$key."] not found.");
        return '';
    }
    return View::render($content, $id, $templateExt, $context);

}
