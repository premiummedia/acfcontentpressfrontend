# Change Log
All notable changes to this project will be documented in this file.

## [1.2.0] - 2017-03-27

### Changed
- added filters for loading values

## [1.1.0] - 2017-03-16

### Changed
- use global post id by default for loading acf field values

### Added
- added isset magic method to allow for checking for variable availability in template

## [1.0.0] - 2017-12-20

### Added
- License file and notices

## [0.4] - 2017-08-22

### Added
- Added defined('ABSPATH') condition to prevent direct execution
- You can add context data to display calls
- Fields keys are shortend to the relevant part for use in templates

### Changed
- Rewrite rendering of ACF components

## [0.3.0] - 2017-05-23

### Added
- Pass id to page template context

## [0.2.3] - 2017-01-26

### Added
- .gitignore file

### Fixed
- Actually use template extension

## [0.2.1] - 2017-01-11

### Changed
- Require php 5.3 instead of 5.4

## [0.2] - 2019-11-15

### Added
- Use different templates for the same ACF component
- Autoloader
